const createRandomDirectoryAndFilesInIt = require("../problem1");

const directoryPath='.//fs_directory';
const numberOfFiles=4;
try{
    createRandomDirectoryAndFilesInIt(directoryPath,numberOfFiles)
    .then(()=>{
        console.log("Created Directory and file in it");
    })
    .catch(error=>{
        console.log(error);
    });
}catch(error){
    console.log(error);
}