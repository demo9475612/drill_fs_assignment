const convertToUpperCaseThenLowerCaseThenSortAndWrite = require("../problem2");
const inputFile ='.//lipsum.txt';
try{
    convertToUpperCaseThenLowerCaseThenSortAndWrite(inputFile)
    .then(()=>{
        console.log("Operation on file is done successfully")
    })
    .catch(error=>{
        console.error('Error Found:',error);
    })
}catch(error){
    console.log(error);
}