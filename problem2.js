const fs = require('fs').promises;

function writeToFile(filePath,content){
    return fs.writeFile(filePath,content);
}

function toReadFile(filePath){
   return fs.readFile(filePath,'utf8');
}

function toAppendFile(toFile,fileName){
    return fs.appendFile(toFile,fileName);
}
function convertToUpperCaseThenLowerCaseThenSortAndWrite(inputFile){
    const fileNames = 'filenames.txt';
    return toReadFile(inputFile)
        .then((inputFileData)=>{
            const upperCaseContent = inputFileData.toUpperCase();
            const upperCaseFile = `upperCase_file.txt`;
            return writeToFile(upperCaseFile,upperCaseContent);
        })
        .then(() =>{
            return toAppendFile(fileNames,'upperCase_file.txt\n')
        })
        .then(()=>{
            return toReadFile(`upperCase_file.txt`);
        })
        .then((upperCaseData)=>{
            const lowerCaseFile= 'lowerCase_file.txt';
            const lowerCaseContent= upperCaseData.toLowerCase();
            const splitContent = lowerCaseContent.split('.');
            const sentences = splitContent.map(sentence=>sentence.trim()).join('.\n');
            return writeToFile(lowerCaseFile,sentences);
        })
        .then(()=>{
            return toAppendFile(fileNames,'lowerCase_file.txt\n')
        })
        .then(()=>{
            return toReadFile('lowerCase_file.txt');
        })
        .then((lowerCaseContent)=>{
            const contentArray = lowerCaseContent.split('\n');
            const sortedContent = contentArray.sort().join('\n');
            return writeToFile('lipsum_sorted.txt',sortedContent);
        })
        .then(()=>{
            return toAppendFile(fileNames,'lipsum_sorted.txt')
        })
        .then(()=>{
            return toReadFile(fileNames);
        })
        .then((listOfFiles)=>{
            const fileListArray=listOfFiles.split('\n');
            const unLinkFiles = fileListArray.map(file=>fs.unlink(file.trim()));
            return Promise.all(unLinkFiles);
        })
        .then(()=>console.log("All files created and deleted successfully"))
        .catch(error=>console.error("Error Found:",error));    
}
module.exports = convertToUpperCaseThenLowerCaseThenSortAndWrite;