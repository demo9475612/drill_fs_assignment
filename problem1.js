const fs = require('fs').promises;
function toWriteInFile(filePath){
     fs.writeFile(filePath,'utf8')
     .then(()=>{
        console.log("File created successfully", filePath);
    })
    .catch(error=>{
        console.error('Error Found',error);
    })
}
function createRandomDirectoryAndFilesInIt(directoryPath, numberOfFiles){
    return fs.mkdir(directoryPath)
    .then(()=>{
    for(let index=1;index<=numberOfFiles;index++){
        const filePath = `${directoryPath}/file${index}.json`;
        toWriteInFile(filePath);
    }
    })
    .then(() => {
    return fs.readdir(directoryPath);
    })
    .then((fileArray)=>{
        const unLinkFiles = fileArray.map(file=>fs.unlink(`${directoryPath}/${file}`));
        return Promise.all(unLinkFiles);
    })
    .catch(error => {
        console.error('Error occurred:', error);
    });
}

module.exports = createRandomDirectoryAndFilesInIt;